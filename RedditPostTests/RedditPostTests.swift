//
//  RedditPostTests.swift
//  RedditPostTests
//
//  Created by gianni murillo anziani on 03-06-21.
//

import XCTest
@testable import RedditPost

class RedditPostTests: XCTestCase {

    var view = HomeMockView()
    static let presenter: VTPHomeProtocol & ITPHomeProtocol = HomePresenter(parameters:[:])
    let interactor: PTIHomeProtocol = HomeInteractor()

    static var fetchTopPostExp: XCTestExpectation!
    static var deleteSinglePostExp: XCTestExpectation!
    static var deleteAllPostsExp: XCTestExpectation!
    static var didReadPostWithSuccessExp: XCTestExpectation!
    
    
    override func setUp() {
        super.setUp()
        RedditPostTests.presenter.view = view
        RedditPostTests.presenter.interactor = interactor
        interactor.presenter = RedditPostTests.presenter
    }
    
    func testFetchTopPosts() {
        RedditPostTests.fetchTopPostExp = expectation(description: "fetchTopPosts")
        RedditPostTests.presenter.fetchPosts()
        waitForExpectations(timeout: 10)
    }
    
    func testDidReadPostWithSuccess() {
        RedditPostTests.didReadPostWithSuccessExp = expectation(description: "didReadPostWithSuccess")
        RedditPostTests.presenter.readPost(id: "")
        waitForExpectations(timeout: 1)
    }
    
    func testDeleteSinglePost() {
        RedditPostTests.deleteSinglePostExp = expectation(description: "deleteSinglePost")
        RedditPostTests.presenter.deletePost(id: "")
        waitForExpectations(timeout: 1)
    }
    
    func testDeleteAllPosts() {
        RedditPostTests.deleteAllPostsExp = expectation(description: "deleteAllPosts")
        RedditPostTests.presenter.deleteAllPosts()
        waitForExpectations(timeout: 1)
    }
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
class HomeMockView:PTVHomeProtocol {
    
    func didDeleteSinglePostsWithSuccess() {
        RedditPostTests.deleteSinglePostExp.fulfill()
        XCTAssertNotNil(RedditPostTests.presenter.deletedPosts)
    }
    
    
    func didDeleteAllPostsWithSuccess() {
        RedditPostTests.deleteAllPostsExp.fulfill()
        XCTAssertTrue(RedditPostTests.presenter.deletedPosts.count == 0)
    }
    
    func didFetchPostWithError(error: String) {
        RedditPostTests.fetchTopPostExp.fulfill()
        XCTFail("didFetchPostWithError: \(error)")
    }
    
    func didReadPostWithSuccess() {
        print("didReadPostWithSuccess")
        RedditPostTests.didReadPostWithSuccessExp.fulfill()
        XCTAssertTrue(true)
    }
    
    func didFetchPostWithSuccess() {
        print("didFetchPostWithSuccess")
        RedditPostTests.fetchTopPostExp.fulfill()
        XCTAssertTrue(true)
    }
}
