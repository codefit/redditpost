README.md# RedditPost

This iOS app is based on VIPER architecture and supports iPad. Created with iOS 14.5

##Home screen 

Uses a UITableView that displays a list of the 50 top reddit's posts. Here you can see 5 main funcionalities:

1. Go to post detail (read post on push)
2. Dismiss one post
3. Dismiss all posts
4. Undo dismiss posts (UIBarButtonItem)
5. Theme (UIBarButtonItem)

Methods in `HomePresenter.swift` handles the functionalities 2,3 and 4 persists the user's data, in this case the readed posts id's and the deleted posts id's -> `HomePresenter.deleted:[String]?` and `HomePresenter.readed:[String]?` arrays

UserDefaults.standard was used to save this data


##Post detail

This is a detailed view of the selected post pushed from `HomeViewController`.
Post image have a `UITapRecognizer` to open a 

##Extra - ThemeManager

`ThemeManager.swift` is a helper class to change between themes (white, dark). You can implement more styles and apply to the app easily with this class. 

Tapping 'Theme' button will toggle the app's style between light and dark modes.


##Dependencies

1. Alamofire: [link](https://github.com/Alamofire/Alamofire)
2. SDWebImage: [link](https://github.com/SDWebImage/SDWebImage)
3. FontAwesome.swift + FontAwesome.otf


