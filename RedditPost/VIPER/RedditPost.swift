//
//  RedditPost.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.

import Foundation

public enum RedditPost: Module {
    case home
    case postDetail
    case split

    public var routePath: String {
        switch self {
        case .home:
            return "RedditPost/Home"
        case .postDetail:
            return "RedditPost/PostDetail"
        case .split:
            return "RedditPost/Split"
        }
    }
}
