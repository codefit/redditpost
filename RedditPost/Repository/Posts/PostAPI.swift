//
//  PostAPI.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.
import Foundation
import Alamofire


class PostAPI {
    
    static func getTopPost(limit:String, completion:@escaping (Result<ResponseGeneric<Post>,AFError>)->Void){
        AlamofireSession.session.request(PostEndpoint.getTopPost(limit: limit))
            .validate()
            .responseDecodable { (response: DataResponse<ResponseGeneric<Post>, AFError>) in
                print(response.debugDescription)
                completion(response.result)
                
            }
    }
}
