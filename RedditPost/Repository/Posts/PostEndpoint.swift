//
//  PostEndpoint.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.

import Foundation
import Alamofire


struct PostRoutes{
    static var top = "top.json"
    
}
struct PostParams{
    static var limit = "limit"
}
enum PostEndpoint: APIConfiguration {

    case getTopPost(limit:String)

    // MARK: - HTTPMethod
    var method: HTTPMethod {
        switch self {
        case .getTopPost:
            return .get
        }
    }
    
    // MARK: - Path
    var path: String {
        switch self {
        case .getTopPost:
            return "\(PostRoutes.top)"
        }
    }
    
    // MARK: - Parameters
    var parameters: Parameters? {
        switch self {
        case .getTopPost(let limit):
            return [PostParams.limit: limit]
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try Endpoints.Environment.baseEnvironment.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .getTopPost:
            var components = URLComponents(string: Endpoints.Environment.baseEnvironment.appending(path))!
            components.queryItems = parameters!.map({ (arg) -> URLQueryItem in
                let (key, value) = arg
                return URLQueryItem(name: key, value: value as? String)
            })
            var urlRequest = URLRequest(url: components.url!)
            urlRequest.httpMethod = method.rawValue
            return urlRequest
        }
//        case .postCase:
//            let url = try Endpoints.Environment.baseAPI.asURL()
//            var urlRequest = URLRequest(url: url.appendingPathComponent(path))
//            urlRequest.httpMethod = method.rawValue
//
//            // Parameters
//            if let parameters = parameters {
//                do {
//                    urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
//                } catch {
//                    throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
//                }
//            }
//            return urlRequest
    }
}
