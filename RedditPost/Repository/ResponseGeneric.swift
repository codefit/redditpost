//
//  ResponseGeneric.swift
//  HighCloud
//
//  Created by gianni murillo anziani on 05-03-21.
//

import Foundation

public struct ResponseGeneric<T: Decodable>: Decodable {
    let data: T
    let kind : String
    
    enum CodingKeys: String, CodingKey {
        case data
        case kind
    }
}
