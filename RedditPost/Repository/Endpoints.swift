//
//  Endpoints.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.

import Foundation

struct Endpoints {
    
    struct Environment {
        
        static let baseEnvironment = Bundle.main.object(forInfoDictionaryKey: "BaseURL") as! String

    }

}
