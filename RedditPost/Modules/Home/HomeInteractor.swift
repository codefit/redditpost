//
//  HomeInteractor.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.

import Foundation

class HomeInteractor:PTIHomeProtocol{

    var presenter: ITPHomeProtocol?
    let userDefaults = UserDefaults.standard
    
    func getPosts(limit:String) {
        PostAPI.getTopPost(limit: limit) { result in
            switch result{
            case .success(let response):
                self.presenter?.didFetchPostWithSuccess(posts: response.data)
            case .failure(let error):
                self.presenter?.didFetchPostWithError(error: error.localizedDescription)
            }
        }
    }
    
    func deleteAllPosts(posts: Post?) {
        let updatedPosts = posts?.children.map({ item in
            return item.data.id
        })
        userDefaults.set(updatedPosts, forKey: "deletedPosts")
        presenter?.didDeleteAllPostsWithSuccess(deletedPosts: updatedPosts ?? [])
    }
    
    func deletePost(id:String){
        
        var updatedDeletedPosts:[String] = []
        if let deletedPosts = userDefaults.object(forKey: "deletedPosts") as? [String]{
            updatedDeletedPosts = deletedPosts
            updatedDeletedPosts.append(id)
        }
        userDefaults.set(updatedDeletedPosts, forKey: "deletedPosts")
        presenter?.didDeleteSinglePostsWithSuccess(deletedPosts: updatedDeletedPosts)
    }
    
    func undoDeleted() {
        
        userDefaults.set([], forKey: "deletedPosts")
        presenter?.didUndoDeletedWithSuccess()
    }
    
    func readPost(id: String) {
        var updatedReadedPosts:[String] = []
        if let readedPosts = userDefaults.object(forKey: "readedPosts") as? [String]{
            updatedReadedPosts = readedPosts
            updatedReadedPosts.append(id)
        }
        userDefaults.set(updatedReadedPosts, forKey: "readedPosts")
        presenter?.didReadPostWithSuccess(readedPosts: updatedReadedPosts)
    }
}
