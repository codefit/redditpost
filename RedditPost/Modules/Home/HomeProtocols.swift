//
//  HomeProtocols.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.


import Foundation

protocol VTPHomeProtocol:AnyObject{
    
    var view: PTVHomeProtocol? {get set}
    var interactor: PTIHomeProtocol? {get set}
    var router: PTRHomeProtocol? {get set}

    var posts:Post? {get set}
    var limit:Int {get set}
    var deletedPosts:[String] {get set}
    var readedPosts:[String] {get set}
    func fetchPosts()
    func deleteAllPosts()
    func deletePost(id:String)
    func readPost(id:String)
    func undoDeleted()
}

protocol PTVHomeProtocol:AnyObject {

    func didFetchPostWithSuccess()
    func didDeleteAllPostsWithSuccess()
    func didDeleteSinglePostsWithSuccess()
    func didFetchPostWithError(error:String)
    func didReadPostWithSuccess()
}

protocol PTRHomeProtocol:AnyObject {
    func navigateToPostDetail(post:Children?)
}

protocol PTIHomeProtocol:AnyObject {

    var presenter:ITPHomeProtocol? {get set}
    func getPosts(limit:String)
    func deleteAllPosts(posts:Post?)
    func deletePost(id:String)
    func readPost(id:String)
    func undoDeleted()
}

protocol ITPHomeProtocol:AnyObject {
    func didFetchPostWithSuccess(posts:Post)
    func didFetchPostWithError(error:String)
    func didDeleteAllPostsWithSuccess(deletedPosts:[String])
    func didDeleteSinglePostsWithSuccess(deletedPosts:[String])
    func didReadPostWithSuccess(readedPosts:[String])
    func didUndoDeletedWithSuccess()
}
