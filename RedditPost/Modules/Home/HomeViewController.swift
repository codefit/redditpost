//
//  HomeViewController.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.

import UIKit

protocol HomeViewControllerProtocol{
    func didSelectPost(post:Children?)
}
class HomeViewController: UIViewController {
    
    @IBOutlet weak var tv_data: UITableView!
    @IBOutlet weak var btn_undoReaded: BaseButton!
    var presenter:VTPHomeProtocol?
    var btnDismissAll:UIBarButtonItem!
    var btnTheme:UIBarButtonItem!
    let refreshControl = UIRefreshControl()
    var selectedIndexPath:IndexPath?
    var delegate:HomeViewControllerProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = ThemeManager.currentTheme().custom_accent
        title = NSLocalizedString("top_posts", comment: "")
        btn_undoReaded.setTitle(NSLocalizedString("no_more_posts", comment: ""),
                                for: .normal)
        btn_undoReaded.setTitleColor(ThemeManager.currentTheme().custom_text_primary,
                                     for: .normal)
        btn_undoReaded.titleLabel?.numberOfLines = 0
        btn_undoReaded.titleLabel?.textAlignment = .center
        btn_undoReaded.center = view.center
        btn_undoReaded.isHidden = true
        refreshControl.addTarget(self, action: #selector(didRefresh(_:)), for: .valueChanged)
        btnDismissAll = UIBarButtonItem(title: NSLocalizedString("dismiss_all", comment: ""),
                                        style: .plain,
                                        target: self,
                                        action: #selector(btn_dismissAllPressed(_:)))
        self.navigationItem.rightBarButtonItem  = btnDismissAll
        
        btnTheme = UIBarButtonItem(title: NSLocalizedString("theme", comment: ""),
                                   style: .plain,
                                   target: self,
                                   action: #selector(btn_themePressed(_:)))
        self.navigationItem.leftBarButtonItem  = btnTheme
        setTableView()
        
        presenter?.fetchPosts()
    }
    @objc private func didRefresh(_ sender: Any) {
        presenter?.fetchPosts()
    }
    @IBAction func btn_showMorePostPressed(_ sender: Any) {
        presenter?.undoDeleted()
    }
    private func setTableView(){
        tv_data.backgroundColor = ThemeManager.currentTheme().custom_background
        tv_data.register(PostItemTVCell.nib,
                         forCellReuseIdentifier: PostItemTVCell.identifier)
        tv_data.refreshControl = refreshControl
        tv_data.delegate = self
        tv_data.dataSource = self
    }
    
    @objc private func btn_themePressed(_ sender:UIBarButtonItem){
        ThemeManager.applyTheme(theme: ThemeManager.currentTheme() == Theme.dark ? Theme.light : Theme.dark)
    }
    @objc private func btn_dismissAllPressed(_ sender:UIBarButtonItem){
        
        let alertController = AlertBuilder.shared.Alert(title: NSLocalizedString("delete_all_post", comment: ""),
                                                        message: NSLocalizedString("delete_all_post_hint", comment: "")) {
            Action.destructive(NSLocalizedString("delete", comment: "")) {
                self.presenter?.deleteAllPosts()
            }
            Action.default(NSLocalizedString("cancel", comment: "")) {}
        }
        UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
    }
    
}
extension HomeViewController:PTVHomeProtocol{
    
    func didDeleteSinglePostsWithSuccess() {
        tv_data.reloadData()
        btn_undoReaded.isHidden = presenter?.posts?.children.count != 0
    }
    
    func didReadPostWithSuccess() {
        
    }
    
    func didDeleteAllPostsWithSuccess() {
        if let indexPath = selectedIndexPath{
            selectedIndexPath = nil
            tv_data.deleteRows(at: [indexPath], with: .left)
            tv_data.reloadData()
        }else{
            tv_data.reloadData()
        }
        btn_undoReaded.isHidden = presenter?.posts?.children.count != 0
    }
    func didFetchPostWithSuccess() {
        refreshControl.endRefreshing()
        tv_data.reloadData()
        btn_undoReaded.isHidden = presenter?.posts?.children.count != 0
    }
    func didFetchPostWithError(error: String) {
        let alertController = AlertBuilder.shared.Alert(title: NSLocalizedString("error", comment: ""),
                                                        message: error) {
            Action.default(NSLocalizedString("ok", comment: "")) {}
        }
        UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
    }
    
}
extension HomeViewController:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return presenter?.posts?.children.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PostItemTVCell.identifier) as! PostItemTVCell
        cell.setView(post: presenter?.posts?.children[indexPath.row].data, isReaded: presenter?.readedPosts.contains(presenter?.posts?.children[indexPath.row].data.id ?? "") ?? false)
        cell.selectionStyle = .none
        cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.readPost(id: presenter?.posts?.children[indexPath.row].data.id ?? "")
        tv_data.reloadRows(at: [indexPath], with: .none)
        if let count = (self.parent?.parent as? SplitViewController)?.viewControllers.count{
            if count == 1{
                presenter?.router?.navigateToPostDetail(post: presenter?.posts?.children[indexPath.row])
            }else{
                delegate?.didSelectPost(post: presenter?.posts?.children[indexPath.row])
            }
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(scaleX: 0.9, y : 0.9)
        UIView.animate(withDuration: 0.25, animations: {
            cell.transform = CGAffineTransform(scaleX: 1, y : 1)
        })
    }
}
extension HomeViewController:PostItemTVCellProtocol{
    func didDeleteItem(with id: String) {
        presenter?.deletePost(id: id)
    }
}
