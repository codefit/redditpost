//
//  PostItemTVCell.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.

import UIKit
import SDWebImage

protocol PostItemTVCellProtocol{
    func didDeleteItem(with id:String)
}
class PostItemTVCell: UITableViewCell {
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    var delegate:PostItemTVCellProtocol?
    @IBOutlet weak var lb_readed: SmallLightLabel!
    @IBOutlet weak var btn_dismiss: TransparentButton!
    @IBOutlet weak var lb_text: MediumLabel!
    @IBOutlet weak var lb_date: MediumLabel!
    @IBOutlet weak var lb_comments: LargeLabel!
    @IBOutlet weak var lb_user: HugeLabel!
    @IBOutlet weak var iv_post: UIImageView!
    var id:String = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.backgroundColor = ThemeManager.currentTheme().custom_background
        btn_dismiss.setTitle(NSLocalizedString("dismiss", comment: ""), for: .normal)
        btn_dismiss.addTarget(self,
                              action: #selector(btn_dismissPostPressed(_:)),
                              for: .touchUpInside)
    }
    
    @objc private func btn_dismissPostPressed(_ sender: UIButton) {
        delegate?.didDeleteItem(with: id)
    }
    func setView(post:PostData?, isReaded:Bool){
        self.id = post?.id ?? ""
        lb_text.text = post?.title ?? ""
        lb_user.text = post?.author ?? ""
        lb_comments.text = String(format: NSLocalizedString("comments_number",
                                                            comment: ""),
                                  String(post?.num_comments ?? 0))
        iv_post.sd_setImage(with: URL(string: post?.thumbnail ?? ""))
        lb_date.text = Date().timeSinceDate(fromDate: Date(timeIntervalSince1970: TimeInterval(post?.created_utc ?? 0)))
        
        if isReaded{
            lb_readed.text = NSLocalizedString("readed", comment: "")
            lb_readed.textColor = ThemeManager.currentTheme().success_text
        }else{
            lb_readed.text = NSLocalizedString("unread", comment: "")
            lb_readed.textColor = ThemeManager.currentTheme().danger_text
        }
        
    }
}
