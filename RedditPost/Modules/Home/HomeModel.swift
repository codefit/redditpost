//
//  HomeModel.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.

import Foundation

struct Post: Codable{

    var children:[Children]
    enum CodingKeys: String, CodingKey {
    	case children
    }
}

struct Children: Codable{

    var kind:String
    var data:PostData
    enum CodingKeys: String, CodingKey {
        case kind
        case data
    }
}

struct PostData: Codable{

    var id: String
    var title: String
    var created_utc: Double
    var num_comments: Int
    var author: String
    var thumbnail: String
    var url: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case created_utc
        case author
        case num_comments
        case thumbnail
        case url
    }
}
