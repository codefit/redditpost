//
//  HomeModule.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.

import UIKit

class HomeModule: IModule {
    let appRouter: IAppRouter
    private var router: HomeRouter!

    init(_ appRouter: IAppRouter) {
        self.appRouter = appRouter
        self.router = HomeRouter(appRouter: self.appRouter)
    }

    func presentView(parameters: [String: Any]) {
        router.presentView(parameters: parameters)
    }

    func createView(parameters: [String: Any]) -> UIViewController? {
        return router.create(parameters: parameters)
    }
}
