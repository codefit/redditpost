//
//  HomePresenter.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.

import Foundation

class HomePresenter:VTPHomeProtocol{

    var view: PTVHomeProtocol?
    var interactor: PTIHomeProtocol?
    var router: PTRHomeProtocol?
    var posts: Post?
    var deletedPosts:[String]
    var readedPosts:[String]
    var limit:Int = 50
    func fetchPosts() {
        interactor?.getPosts(limit: String(limit))
    }
    func deleteAllPosts(){
        interactor?.deleteAllPosts(posts: posts)
    }
    func deletePost(id: String) {
        interactor?.deletePost(id: id)
    }
    func undoDeleted() {
        interactor?.undoDeleted()
    }
    func readPost(id: String) {
        interactor?.readPost(id: id)
    }
    init(parameters:[String:Any]) {
        
        deletedPosts = []
        readedPosts = []
    }
}

extension HomePresenter:ITPHomeProtocol{
    
    func didDeleteSinglePostsWithSuccess(deletedPosts: [String]) {
        self.deletedPosts = deletedPosts
        posts?.children.removeAll(where: { post in
            return self.deletedPosts.contains(post.data.id)
        })
        view?.didDeleteSinglePostsWithSuccess()
    }
    
    func didReadPostWithSuccess(readedPosts: [String]) {
        self.readedPosts = readedPosts
        view?.didReadPostWithSuccess()
    }
    func didDeleteAllPostsWithSuccess(deletedPosts:[String]) {
        self.deletedPosts = deletedPosts
        posts?.children.removeAll(where: { post in
            return self.deletedPosts.contains(post.data.id)
        })
        view?.didDeleteAllPostsWithSuccess()
    }
    func didFetchPostWithSuccess(posts: Post) {

        self.posts = posts
        
        let userDefaults = UserDefaults.standard
        if let deletedIds = userDefaults.object(forKey: "deletedPosts") as? [String]{
            deletedPosts = deletedIds
        }else{
            userDefaults.setValue([], forKey: "deletedPosts")
            deletedPosts = []
        }
        
        if let readedIds = userDefaults.object(forKey: "readedPosts") as? [String]{
            readedPosts = readedIds
        }else{
            userDefaults.setValue([], forKey: "readedPosts")
            deletedPosts = []
        }
        self.posts?.children.removeAll(where: { post in
            return self.deletedPosts.contains(post.data.id)
        })
        view?.didFetchPostWithSuccess()
    }
    func didFetchPostWithError(error: String) {
        view?.didFetchPostWithError(error: error)
    }
    func didUndoDeletedWithSuccess() {
        deletedPosts = []
        fetchPosts()
    }
}
