//
//  PostDetailItemTVCell.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.

import UIKit

protocol PostDetailItemTVCellProtocol: AnyObject {
    
}

class PostDetailItemTVCell: UITableViewCell {
    
    @IBOutlet weak var lb_info: MediumLabel!
    @IBOutlet weak var lb_author: HugeLabel!
    @IBOutlet weak var lb_text: MediumLabel!
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lb_info.text = ""
        contentView.backgroundColor = ThemeManager.currentTheme().custom_background
    }
    
    func setView(post:Children?){
        
        lb_author.text =  post?.data.author ?? ""
        lb_text.text = post?.data.title ?? ""
        
        guard let date = post?.data.created_utc else{return}
        lb_info.text = Date().timeSinceDate(fromDate: Date(timeIntervalSince1970: TimeInterval(date))) + " - " + String(format: NSLocalizedString("comments_number",comment: ""), String(post?.data.num_comments ?? 0))
        contentView.backgroundColor = ThemeManager.currentTheme().custom_white
    }
}

extension PostDetailItemTVCell: PostDetailItemTVCellProtocol {
    
}
