//
//  PostDetailProtocols.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.


import Foundation

protocol VTPPostDetailProtocol:AnyObject{
    
    var view: PTVPostDetailProtocol? {get set}
    var interactor: PTIPostDetailProtocol? {get set}
    var router: PTRPostDetailProtocol? {get set}
    var post: Children? {get set}
}

protocol PTVPostDetailProtocol:AnyObject {

}

protocol PTRPostDetailProtocol:AnyObject {
    
}

protocol PTIPostDetailProtocol:AnyObject {

    var presenter:ITPPostDetailProtocol? {get set}
    
}

protocol ITPPostDetailProtocol:AnyObject {

}
