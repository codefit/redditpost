//
//  PostDetailPresenter.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.

import Foundation

class PostDetailPresenter:VTPPostDetailProtocol{
        
    var view: PTVPostDetailProtocol?
    var interactor: PTIPostDetailProtocol?
    var router: PTRPostDetailProtocol?
    var post:Children?
    
    init(parameters:[String:Any]) {
        post = parameters["post"] as? Children
    }
}

extension PostDetailPresenter:ITPPostDetailProtocol{

}
