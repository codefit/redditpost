//
//  PostDetailRouter.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.

import UIKit

class PostDetailRouter:PTRPostDetailProtocol{
    
    var appRouter: IAppRouter

    init(appRouter: IAppRouter) {
        self.appRouter = appRouter
    }

    func presentView(parameters: [String: Any]) {
        appRouter.presentView(view: create(parameters: parameters))
    }

    func create(parameters: [String: Any]) -> PostDetailViewController {

        let bundle = Bundle(for: type(of: self))
        let view = PostDetailViewController(nibName: "PostDetailViewController", bundle: bundle)        
        let presenter: VTPPostDetailProtocol & ITPPostDetailProtocol = PostDetailPresenter(parameters:parameters)
        let interactor: PTIPostDetailProtocol = PostDetailInteractor()
        let router:PTRPostDetailProtocol = PostDetailRouter(appRouter: appRouter)
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
        
    }
}
