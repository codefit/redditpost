//
//  PostDetailViewController.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.

import UIKit
import SafariServices

class PostDetailViewController: UIViewController {

    var presenter:VTPPostDetailProtocol?
    var iv_event:UIImageView = UIImageView()
    var imageHeight:CGFloat = UIScreen.main.bounds.width * 0.4
    var tap: UITapGestureRecognizer!
    @IBOutlet weak var tv_data: UITableView!
    var topbarHeight: CGFloat = 70
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setTableView()
        setImageView()
        view.addSubview(iv_event)
    
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //iv_event.removeGestureRecognizer(tap)
    }
    private func setImageView(){
        tap = UITapGestureRecognizer(target: self, action: #selector(imagePressed(_:)))
        iv_event.addGestureRecognizer(tap)
        iv_event.isUserInteractionEnabled = true
        iv_event.frame = CGRect(x: 0,
                                y: topbarHeight,
                                width: view.frame.size.width,
                                height: imageHeight)
        iv_event.contentMode = .scaleAspectFit
        iv_event.backgroundColor = .clear
        iv_event.clipsToBounds = true
        iv_event.sd_setImage(with: URL(string: (presenter?.post?.data.thumbnail  ?? ""))) { (image, error, cache, url) in
            
            self.iv_event.frame = self.iv_event.contentClippingRect
            self.iv_event.frame.origin.y = self.topbarHeight
            self.imageHeight = self.iv_event.frame.size.height
        }
    }
    private func setTableView(){
        tv_data.backgroundColor = ThemeManager.currentTheme().custom_background
        tv_data.delegate = self
        tv_data.dataSource = self
        tv_data.estimatedRowHeight = 450
        tv_data.rowHeight = UITableView.automaticDimension
        tv_data.register(PostDetailItemTVCell.nib,
                         forCellReuseIdentifier: PostDetailItemTVCell.identifier)
        tv_data.contentInset = UIEdgeInsets(top: imageHeight,
                                            left: 0,
                                            bottom: 0,
                                            right: 0)
    }
    
    @objc private func imagePressed(_ sender:UIImageView){
        
        guard let url = URL(string: presenter?.post?.data.url ?? "http://reddit.com") else {return}
        let safariVC = SFSafariViewController(url: url)
        self.present(safariVC, animated: true, completion: nil)
    }
    
    func setPost(post:Children?){
        presenter?.post = post
        setImageView()
        tv_data.reloadData()
    }
}
extension PostDetailViewController:PTVPostDetailProtocol{

}
extension PostDetailViewController:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PostDetailItemTVCell.identifier) as! PostDetailItemTVCell
        cell.setView(post: presenter?.post)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = imageHeight - CGFloat((scrollView.contentOffset.y + imageHeight + topbarHeight))
        let height = min(max(y, 60), 300)
        self.iv_event.frame = self.iv_event.contentClippingRect
        
        iv_event.frame = CGRect(x: 0,
                                y: topbarHeight,
                                width: view.frame.size.width,
                                height: imageHeight)
        iv_event.frame.size.height = height
    }
    
}
