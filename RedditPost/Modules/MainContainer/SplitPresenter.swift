//
//  SplitPresenter.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.

import Foundation

class SplitPresenter:VTPSplitProtocol{
        
    var view: PTVSplitProtocol?
    var interactor: PTISplitProtocol?
    var router: PTRSplitProtocol?

    init(parameters:[String:Any]) {

    }
}

extension SplitPresenter:ITPSplitProtocol{

}
