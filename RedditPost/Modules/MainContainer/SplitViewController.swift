//
//  SplitViewController.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.

import UIKit

class SplitViewController: UISplitViewController, UISplitViewControllerDelegate {
    
    var presenter:VTPSplitProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        self.view.backgroundColor = ThemeManager.currentTheme().custom_accent
        preferredDisplayMode = UISplitViewController.DisplayMode.oneBesideSecondary
        var tabBarList:[UIViewController] = []
        if let module = AppRouter.share.modules[RedditPost.home.routePath] {
            let module = module(AppRouter.share)
            let vc = module.createView(parameters: [:])! as! HomeViewController
            vc.delegate = self
            let nav = UINavigationController(rootViewController: vc)
            nav.title = NSLocalizedString("detail", comment: "")
            
            tabBarList.append(nav)
        }
        if let module = AppRouter.share.modules[RedditPost.postDetail.routePath] {
            let module = module(AppRouter.share)
            let vc = module.createView(parameters: [:])!
            let nav = UINavigationController(rootViewController: vc)
            nav.title = NSLocalizedString("detail", comment: "")
            tabBarList.append(nav)
        }
        viewControllers = tabBarList
    }
    func splitViewController(
        _ splitViewController: UISplitViewController,
        collapseSecondary secondaryViewController: UIViewController,
        onto primaryViewController: UIViewController) -> Bool {
        // Return true to prevent UIKit from applying its default behavior
        return true
    }
    func splitViewController(_ svc: UISplitViewController, topColumnForCollapsingToProposedTopColumn proposedTopColumn: UISplitViewController.Column) -> UISplitViewController.Column {
        return .primary
    }
}
extension SplitViewController:PTVSplitProtocol{
    
}
extension SplitViewController:HomeViewControllerProtocol{
    
    func didSelectPost(post: Children?) {
        let navVC = viewControllers.last as? UINavigationController
        guard let vc = navVC?.viewControllers.first as? PostDetailViewController else {  return }
        vc.setPost(post: post)
    }
}
