//
//  SplitRouter.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.

import UIKit

class SplitRouter:PTRSplitProtocol{
    
    var appRouter: IAppRouter

    init(appRouter: IAppRouter) {
        self.appRouter = appRouter
    }

    func presentView(parameters: [String: Any]) {
        appRouter.presentView(view: create(parameters: parameters))
    }

    func create(parameters: [String: Any]) -> SplitViewController {

        let bundle = Bundle(for: type(of: self))
        let view = SplitViewController(nibName: "SplitViewController", bundle: bundle)        
        let presenter: VTPSplitProtocol & ITPSplitProtocol = SplitPresenter(parameters:parameters)
        let interactor: PTISplitProtocol = SplitInteractor()
        let router:PTRSplitProtocol = SplitRouter(appRouter: appRouter)
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
        
    }
}