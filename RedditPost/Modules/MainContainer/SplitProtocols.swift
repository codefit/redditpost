//
//  SplitProtocols.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.


import Foundation

protocol VTPSplitProtocol:AnyObject{
    
    var view: PTVSplitProtocol? {get set}
    var interactor: PTISplitProtocol? {get set}
    var router: PTRSplitProtocol? {get set}

}

protocol PTVSplitProtocol:AnyObject {

}

protocol PTRSplitProtocol:AnyObject {
    
}

protocol PTISplitProtocol:AnyObject {

    var presenter:ITPSplitProtocol? {get set}
    
}

protocol ITPSplitProtocol:AnyObject {

}
