//
//  ThemeManager.swift
//  RedditPost
//
//  Created by gianni murillo anziani on 03-06-21.
//  Copyright (c) 2021 Gianni Murillo Anziani. All rights reserved.


import Foundation
import UIKit

enum Theme: Int {
    
    case light, dark
    
    var custom_black: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0x000000)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0xFFFFFF)
        }
    }
    
    var custom_white: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0xFFFFFF)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0x000000)
        }
    }

    var custom_primary: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0xFE4500)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0xFE4500)
        }
    }
    
    var custom_accent: UIColor {
        switch self {
        case .light:
            return custom_primary
        case .dark:
            return custom_primary
        }
    }
    
    var custom_accent_text: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0xFFFFFF)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0xFFFFFF)
        }
    }
    
    var custom_background: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0x666666)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0x1d0505)
        }
    }
    
    var custom_text_primary: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0x5E5E5E)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0x5E5E5E)
        }
    }
    
    var custom_text_secondary: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0xADADAD)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0xADADAD)
        }
    }
    
    var success: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0x8AC926)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0x8AC926)
        }
    }
    
    var success_text: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0x56c44a)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0x56c44a)
        }
    }
    
    var danger: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0xEF233C)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0xEF233C)
        }
    }
    
    var danger_text: UIColor {
        switch self {
        case .light:
            return custom_accent
        case .dark:
            return custom_accent
        }
    }

    var button_background: UIColor {
        switch self {
        case .light:
            return custom_primary
        case .dark:
            return custom_primary
        }
    }
    
    var button_text: UIColor {
        switch self {
        case .light:
            return custom_white
        case .dark:
            return custom_white
        }
    }
    
    var toolbar: UIColor {
        switch self {
        case .light:
            return custom_accent
        case .dark:
            return custom_accent
        }
    }
    
    var toolbar_text: UIColor {
        switch self {
        case .light:
            return custom_black
        case .dark:
            return custom_black
        }
    }
    
    var toolbar_accent: UIColor {
        switch self {
        case .light:
            return custom_black
        case .dark:
            return custom_black
        }
    }

    
    
    var barStyle: UIBarStyle {
        switch self {
        case .light:
            return .default
        case .dark:
            return .black
        }
    }
}

// Enum declaration
let SelectedThemeKey = "SelectedTheme"

// This will let you use a theme in the app.
class ThemeManager {
    
    // ThemeManager
    static func currentTheme() -> Theme {
        if let storedTheme = (UserDefaults.standard.value(forKey: SelectedThemeKey) as AnyObject).integerValue {
            return Theme(rawValue: storedTheme)!
        } else {
            return .light
        }
    }
    
    static func applyTheme(theme: Theme) {

        setRootViewController()
        // First persist the selected theme using NSUserDefaults.
        UserDefaults.standard.setValue(theme.rawValue, forKey: SelectedThemeKey)
        UserDefaults.standard.synchronize()
        
        UITabBar.appearance().tintColor = ThemeManager.currentTheme().custom_accent
        
        if #available(iOS 13.0, *) {
            UINavigationBar.appearance().barStyle = theme.barStyle
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.backgroundColor = theme.toolbar
            navBarAppearance.titleTextAttributes = [.foregroundColor: theme.toolbar_text]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: theme.toolbar_text]
            let navigationBarAppearace = UINavigationBar.appearance()
            navigationBarAppearace.tintColor = ThemeManager.currentTheme().toolbar_accent
            navigationBarAppearace.barTintColor = ThemeManager.currentTheme().toolbar_accent
            UINavigationBar.appearance(whenContainedInInstancesOf: [UINavigationController.self]).standardAppearance = navBarAppearance
            UINavigationBar.appearance(whenContainedInInstancesOf: [UINavigationController.self]).scrollEdgeAppearance = navBarAppearance
        } else {
            UINavigationBar.appearance().barStyle = theme.barStyle
            UINavigationBar.appearance().titleTextAttributes =
                [NSAttributedString.Key.foregroundColor: theme.custom_black,
                 NSAttributedString.Key.font: TypoManager.currentTypo().bold(size: 16)]
        }

    }
    
    static func setRootViewController(){
        AppRouter.share.presentModule(module: RedditPost.split, type: .root)
    }
}

